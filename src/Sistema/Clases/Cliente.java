package Sistema.Clases;

/**
 * Created by juancarlos on 07/07/2017.
 */
public class Cliente {

    private String NombreCliente;
    private int TelefonoCliente;
    private String DireccionCliente;

    public Cliente() {
    }

    public Cliente(String nombreCliente, int telefonoCliente, String direccionCliente) {
        NombreCliente = nombreCliente;
        TelefonoCliente = telefonoCliente;
        DireccionCliente = direccionCliente;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        NombreCliente = nombreCliente;
    }

    public int getTelefonoCliente() {
        return TelefonoCliente;
    }

    public void setTelefonoCliente(int telefonoCliente) {
        TelefonoCliente = telefonoCliente;
    }

    public String getDireccionCliente() {
        return DireccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        DireccionCliente = direccionCliente;
    }
}
