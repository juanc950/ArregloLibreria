package Sistema.Clases;

/**
 * Created by juancarlos on 07/07/2017.
 */
public class Libros {

    private String NombreLibro;
    private String editorial;

    public Libros() {
    }

    public Libros(String nombreLibro, String editorial) {
        NombreLibro = nombreLibro;
        this.editorial = editorial;
    }

    public String getNombreLibro() {
        return NombreLibro;
    }

    public void setNombreLibro(String nombreLibro) {
        NombreLibro = nombreLibro;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
}
