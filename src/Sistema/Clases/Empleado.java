package Sistema.Clases;

/**
 * Created by juancarlos on 07/07/2017.
 */
public class Empleado {
    private String NombreEmpleado;
    private int TelefonoEmpleado;
    private String DireccionEmpleado;

    public Empleado() {
    }

    public Empleado(String nombreEmpleado, int telefonoEmpleado, String direccionEmpleado) {
        NombreEmpleado = nombreEmpleado;
        TelefonoEmpleado = telefonoEmpleado;
        DireccionEmpleado = direccionEmpleado;
    }

    public String getNombreEmpleado() {
        return NombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        NombreEmpleado = nombreEmpleado;
    }

    public int getTelefonoEmpleado() {
        return TelefonoEmpleado;
    }

    public void setTelefonoEmpleado(int telefonoEmpleado) {
        TelefonoEmpleado = telefonoEmpleado;
    }

    public String getDireccionEmpleado() {
        return DireccionEmpleado;
    }

    public void setDireccionEmpleado(String direccionEmpleado) {
        DireccionEmpleado = direccionEmpleado;
    }
}
